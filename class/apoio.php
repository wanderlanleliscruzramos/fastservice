<?php
session_start();
$_SESSION['oferta'] = $_GET['oferta'];
$profissional 		= $_GET['profissional'];
$cliente 			= $_SESSION['id'];
$tipo 				= $_GET['tipo'];
$oferta 			= $_GET['oferta'];
$contrato 			= $_GET['contrato'];

switch ($tipo) {
	case 'profissional':
		require_once'profissional.php';
		$profissional = new profissional();
		$profissional->solicitacaoAceitar();
		break;

	case 'contrato':
		if (is_null($cliente) or is_null($profissional) ) 
		{
			echo "Error"; 
		}
		else
		{	require_once'contrato.php';
			$contrato = new contrato();
			$contrato->_cliente		= $cliente;
			$contrato->_oferta		= $_SESSION['oferta'];
			$contrato->_profissional= $profissional;
			$contrato->contratar();
		}
		break;
}
?>