<?php
class Conexao
{
	private $hostname = 'localhost';
	private $usuario  = 'root';
	private $senha 	  = '';
	private $db 	  = 'fastservices';
	private $porta	  = '';

	function __construct()
	{
		$this->conectar();
	}	
	public function conectar()
	{
		$this->con = new mysqli($this->hostname,
								$this->usuario,
								$this->senha,
								$this->db);

		if (mysqli_connect_errno()){
				echo "Falha ao tentar se conectar ao banco: " . mysqli_connect_error(); 
			}
	}
	public function close()
	{
		mysqli_close($this->con);
	}	
}
?>