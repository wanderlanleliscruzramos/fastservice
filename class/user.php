<?php
class User
{	
	# mesagens de informação
	public $msgUserInativo	 = "Usuario inativo, entre em contato com o administrador do sistema.";
	public $msgUserLogon  	 = "Usuario e/ou senha incorreto, tente novamente.";

	# dados do usuario	
	public 	$_id;
	public 	$_nome;
	public 	$_sobrenome;
	public 	$_apelido;
	public  $_email;
	private $_usuario;
	private $_senha;
	private $_ContraSenha;
	public 	$_status;
	public 	$_perfil;

	function __construct()
	{
		require_once('conexao.php');
	}
	public function setUsuario($usuario){ $this->_usuario	= $usuario;}
	public function setSenha($senha){ $this->_senha 	= $senha;}
	public function setContraSenha($contrasenha){ $this->_ContraSenha 	= $contrasenha;}


	public function getUsuario(){ return $this->_usuario; }
	public function getSenha(){ return $this->_senha; }
	public function getContraSenha(){ return $this->_ContraSenha; }

	public function logOn()
	{			
		$conexao 			 = new Conexao();
		$query 	 			 = "SELECT * FROM USERS WHERE USUARIO='$this->_usuario' AND SENHA='$this->_senha'";
		$this->select 			 = $conexao->con->query($query);
		$quantidade 			 = mysqli_num_rows($this->select);
		$row 				 = mysqli_fetch_assoc($this->select);

		if (mysqli_num_rows($this->select) == 1) 
		{				
			if($row['status'] == 1)
			{	
				session_start();
				$_SESSION['id']			= $row['id'];
				$_SESSION['nome'] 	 	= $row['nome'];
				$_SESSION['sobrenome'] 		= $row['sobrenome'];
				$this->_apelido 		= $row['apelido'];
				$_SESSION['usuario']		= $row['usuario']; 
				$this->_status 	 		= $row['status'];
				$this->_perfil 	 		= $row['perfil_id'];

				switch ($this->_perfil) {
					case 1:
						header('location: ../view/oferta.php');
						break;

					case 2:	
						header('location: ../view/painelProfissional.php');
						break;
				}
				
			}
			else echo $this->msgUserInativo; 			
		}else echo $this->msgUserLogon;
	}
	public function logOUt()
	{
		
	    	unset($_SESSION["nome"]);
	    	session_destroy();
	    	require_once('../config.php');
		header("Location: ".$source.'../viewer/userLogon.php');
	}
	public function cadastro()
	{
		require_once('validacao.php');
		$validacao = new Validacao();
		$validacao->setSenha($this->_senha );
		$validacao->setContraSenha($this->_ContraSenha );
		$validacao->validarSenha();

		$conexao = new Conexao();
		$query 	 = "INSERT INTO users VALUES (	NULL,
							'$this->_nome',
							'$this->_sobrenome',
							'$this->_apelido',
							'$this->_email',
							'$this->_usuario',
							'$this->_senha',1,
							'$this->_perfil')";

		$conexao->con->query($query);
		if(mysqli_affected_rows($conexao->con))
			{
				echo $this->msgInsert;
				header("Location: ".$source.'../viewer/userLogon.php');
			}
		$conexao->close();
	}
	public function atualizar($id,$cn,$login,$senha)
	{
		$conexao 		= new Conexao();
		$query 	 		= "UPDATE USERS SET cn='$cn', logonName='$login', senha='$senha' WHERE idusers = '$id'";
		$this->update 	= $conexao->con->query($query);
		if($this->update)
		{
			echo $this->msgUpdate;
		}else echo $this->msgErroUpdate;
	}
	public function desativar($id)
	{
		$conexao 		= new Conexao();
		$query 	 		= "UPDATE USERS SET status=0 WHERE idusers='$id' ";
		$this->cancelar = $conexao->con->query($query);

		if($this->cancelar)
		{
			echo $this->msgDelet;
		}else echo $this->msgErroDelet;
	}
}
?>