-- MySQL Script generated by MySQL Workbench
-- 05/07/16 20:56:06
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema fastServices
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema fastServices
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `fastServices` DEFAULT CHARACTER SET utf8 ;
USE `fastServices` ;

-- -----------------------------------------------------
-- Table `fastServices`.`perfil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`perfil` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `sobrenome` VARCHAR(45) NULL,
  `apelido` VARCHAR(45) NULL,
  `email` VARCHAR(100) NULL,
  `usuario` VARCHAR(45) NULL,
  `senha` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `perfil_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_users_perfil1_idx` (`perfil_id` ASC),
  CONSTRAINT `fk_users_perfil1`
    FOREIGN KEY (`perfil_id`)
    REFERENCES `fastServices`.`perfil` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`contato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`contato` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `telefone` INT NULL,
  `celular` INT NULL,
  `email` VARCHAR(45) NULL,
  `site` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_contato_users_idx` (`users_id` ASC),
  CONSTRAINT `fk_contato_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `fastServices`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`adress`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`adress` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `logradouro` VARCHAR(70) NULL,
  `numero` INT NULL,
  `bairro` VARCHAR(45) NULL,
  `cidade` VARCHAR(45) NULL,
  `uf` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_adress_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_adress_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `fastServices`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`profissional`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`profissional` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `descricao` VARCHAR(255) NULL,
  `satisfacao` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_profissional_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_profissional_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `fastServices`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`notificacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`notificacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `profissional_id` INT NOT NULL,
  `mensagem` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_notificacao_users1_idx` (`users_id` ASC),
  INDEX `fk_notificacao_profissional1_idx` (`profissional_id` ASC),
  CONSTRAINT `fk_notificacao_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `fastServices`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_notificacao_profissional1`
    FOREIGN KEY (`profissional_id`)
    REFERENCES `fastServices`.`profissional` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`categoriaOferta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`categoriaOferta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `descricao` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`oferta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`oferta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `profissional_id` INT NOT NULL,
  `categoriaOferta_id` INT NOT NULL,
  `titulo` VARCHAR(45) NULL,
  `descricao` VARCHAR(45) NULL,
  `imagem` VARCHAR(45) NULL,
  `valor` DOUBLE NULL,
  `cadastro` VARCHAR(45) NULL,
  `validade` DATE NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_oferta_profissional1_idx` (`profissional_id` ASC),
  INDEX `fk_oferta_categoriaOferta1_idx` (`categoriaOferta_id` ASC),
  CONSTRAINT `fk_oferta_profissional1`
    FOREIGN KEY (`profissional_id`)
    REFERENCES `fastServices`.`profissional` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_oferta_categoriaOferta1`
    FOREIGN KEY (`categoriaOferta_id`)
    REFERENCES `fastServices`.`categoriaOferta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`contratos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`contratos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `oferta_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `profissional_id` INT NOT NULL,
  `cadastro` DATE NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_contratos_oferta1_idx` (`oferta_id` ASC),
  INDEX `fk_contratos_profissional1_idx` (`profissional_id` ASC),
  INDEX `fk_contratos_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_contratos_oferta1`
    FOREIGN KEY (`oferta_id`)
    REFERENCES `fastServices`.`oferta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contratos_profissional1`
    FOREIGN KEY (`profissional_id`)
    REFERENCES `fastServices`.`profissional` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contratos_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `fastServices`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`habilidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`habilidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `profissional_id` INT NOT NULL,
  `descricao` VARCHAR(255) NULL,
  `habilidadecol` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_hablilidade_profissional1_idx` (`profissional_id` ASC),
  CONSTRAINT `fk_hablilidade_profissional1`
    FOREIGN KEY (`profissional_id`)
    REFERENCES `fastServices`.`profissional` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `fastServices`.`satisfacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `fastServices`.`satisfacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `profissional_id` INT NOT NULL,
  `tipo` INT NULL,
  `comentario` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_satisfacao_users1_idx` (`users_id` ASC),
  INDEX `fk_satisfacao_profissional1_idx` (`profissional_id` ASC),
  CONSTRAINT `fk_satisfacao_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `fastServices`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_satisfacao_profissional1`
    FOREIGN KEY (`profissional_id`)
    REFERENCES `fastServices`.`profissional` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
